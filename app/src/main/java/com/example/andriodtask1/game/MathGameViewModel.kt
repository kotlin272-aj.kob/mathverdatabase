package com.example.andriodtask1.game

import android.app.Application
import android.os.CountDownTimer
import androidx.lifecycle.*
import com.example.andriodtask1.database.player.Player
import com.example.andriodtask1.database.player.PlayerDatabaseDao
import com.example.andriodtask1.datamodel.Question
import kotlinx.coroutines.launch

class MathGameViewModel(
    menu: Int,
    val playerId:Long,
    val database: PlayerDatabaseDao,
    application: Application
) : ViewModel(){
    private val _player = MutableLiveData<Player>()
    val player : LiveData<Player>
        get() = _player

    private val _question = MutableLiveData<Question>()
    val question : LiveData<Question>
        get()=_question

    private var _menu = MutableLiveData<Int>()
    val menu : LiveData<Int>
        get()= _menu

    private val _isAddScore = MutableLiveData<Boolean>()
    val isAddScore : LiveData<Boolean>
        get() = _isAddScore

    private val _eventNewGame = MutableLiveData<Boolean>()
    val eventNewGame : LiveData<Boolean>
        get() = _eventNewGame

    private val _eventChooseAnswer1 = MutableLiveData<Boolean>()
    val eventChooseAnswer1 : LiveData<Boolean>
        get() = _eventChooseAnswer1

    private val _eventChooseAnswer2 = MutableLiveData<Boolean>()
    val eventChooseAnswer2 : LiveData<Boolean>
        get() = _eventChooseAnswer2

    private val _eventChooseAnswer3 = MutableLiveData<Boolean>()
    val eventChooseAnswer3 : LiveData<Boolean>
        get() = _eventChooseAnswer3

    private val _eventUpdateResult = MutableLiveData<Int>()
    val eventUpdateResult : LiveData<Int>
        get() = _eventUpdateResult

    private val _questionTime = MutableLiveData<Long>()
    val questionTime : LiveData<Long>
        get() = _questionTime

    private var questionTimer: CountDownTimer?

    val currentQuestionTimeString = Transformations.map(questionTime) { time ->
        _questionTime.value?.toInt()
    }
    fun getScoreValue() = _player.value
    fun getQuestionValue() = _question.value

    fun onChooseAnswer1() {
        updateResult(1)
        _eventChooseAnswer1.value = true
    }
    fun onChooseAnswer2() {
        updateResult(2)
        _eventChooseAnswer2.value = true
    }
    fun onChooseAnswer3() {
        updateResult(3)
        _eventChooseAnswer3.value = true
    }
    private fun onResultCorrect() {
        _eventUpdateResult.value = 1
    }
    private fun onResultCorrectNotAdd() {
        _eventUpdateResult.value = 2
    }
    private fun onResultWrong() {
        _eventUpdateResult.value = 3
    }

    private fun updateResult(choiceNumber:Int){
        val question = getQuestionValue()
        if (question?.checkAnswerCorrect(choiceNumber)!!) {
            if (checkCanAddScore()) {
                addAmountCorrect()
                onResultCorrect()
            }else{
                onResultCorrectNotAdd()
            }
        } else {
            if (checkCanAddScore()) {
                addAmountWrong()
                onResultWrong()
            }
        }

        viewModelScope.launch {
            database.update(player.value!!)
        }
        _isAddScore.value = true
    }

    private fun checkCanAddScore() : Boolean{
        return !isAddScore.value!!
    }
    fun onChooseComplete() {
        _eventChooseAnswer1.value = false
        _eventChooseAnswer2.value = false
        _eventChooseAnswer3.value = false
    }
    fun startTimeQuestion() {
        questionTimer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                _questionTime.value = millisUntilFinished / 1000L
            }
            override fun onFinish() {
                _questionTime.value = 0L
                _question.value?.msgResult = ""
                newQuestion()
            }
        }
        questionTimer?.start()
    }
    fun stopTimeQuestion() {
        _questionTime.value = 0L
        questionTimer?.cancel()
    }

    private fun addAmountCorrect() {
        if(!_isAddScore.value!!){
            _player.value?.addCorrect()
        }
        _isAddScore.value = true
    }

    private fun addAmountWrong() {
        _player.value?.addWrong()
        _isAddScore.value = true
    }

    fun newQuestion(){
        _question.value?.createQuestionAndResult(_menu.value!!)
        _question.value?.createChoice()
        _isAddScore.value = false
        _eventNewGame.value = true
    }
    fun newGameComplete(){
        _eventNewGame.value = false
    }
    fun setMenu(menu:Int){
        _menu.value = menu
    }

    init {
        viewModelScope.launch {
            _player.value = database.get(playerId) ?: Player()
        }

        questionTimer = null
        _menu.value = menu

        _question.value = Question()
        question.value?.createQuestionAndResult(_menu.value!!)
        question.value?.createChoice()
        _isAddScore.value = false
    }
}