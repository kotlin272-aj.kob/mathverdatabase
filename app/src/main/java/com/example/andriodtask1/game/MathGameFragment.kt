package com.example.andriodtask1.game

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.andriodtask1.R
import com.example.andriodtask1.choosemap.ChooseMapViewModelFactory
import com.example.andriodtask1.database.player.PlayerDatabase
import com.example.andriodtask1.databinding.FragmentMathGameBinding


class MathGameFragment : Fragment() {
    lateinit var binding: FragmentMathGameBinding

    private lateinit var gameViewModel: MathGameViewModel
    private lateinit var gameViewModelFactory: MathGameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMathGameBinding>(
            inflater,
            R.layout.fragment_math_game  , container, false
        )

        createFactoryAndGetScore()
        gameViewModel = ViewModelProvider(this,gameViewModelFactory).get(MathGameViewModel::class.java)

        gameViewModel.eventNewGame.observe(viewLifecycleOwner, Observer { hasNewGame ->
            if(hasNewGame){
                mainGame()
                gameViewModel.newGameComplete()
            }
        })
        gameViewModel.eventChooseAnswer1.observe(viewLifecycleOwner, Observer { hasChoose ->
            if(hasChoose){
                updateBtnAnswer(1)
                gameViewModel.onChooseComplete()
            }
        })
        gameViewModel.eventChooseAnswer2.observe(viewLifecycleOwner, Observer { hasChoose ->
            if(hasChoose){
                updateBtnAnswer(2)
                gameViewModel.onChooseComplete()
            }
        })
        gameViewModel.eventChooseAnswer3.observe(viewLifecycleOwner, Observer { hasChoose ->
            if(hasChoose){
                updateBtnAnswer(3)
                gameViewModel.onChooseComplete()
            }
        })
        gameViewModel.eventUpdateResult.observe(viewLifecycleOwner, Observer { result ->
            if(result === 1){
                gameViewModel.question.value?.msgResult = context?.resources?.getString(R.string.result_correct).toString()
            }else if(result === 2){
                gameViewModel.question.value?.msgResult = context?.resources?.getString(R.string.result_correct_not_add).toString()
            }else if(result === 3){
                gameViewModel.question.value?.msgResult = context?.resources?.getString(R.string.result_wrong).toString()
            }
        })
        binding.gameViewModel = gameViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        mainGame()
        setBtnBackToMain()
        gameViewModel.question.value?.operator = getOperator()

        return binding.root
    }

    private fun mainGame() {
        resetStartComponent()
        resetStartBackground()
        binding.invalidateAll()
    }

    private fun createFactoryAndGetScore() {
        val args =
            MathGameFragmentArgs.fromBundle(
                requireArguments()
            )
        val application = requireNotNull(requireActivity()).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val chooseMapViewModelFactory = MathGameViewModelFactory(args.menu,args.playerId,dataSource,application)

        gameViewModelFactory = MathGameViewModelFactory(
            args.menu,
            args.playerId,
            dataSource,
            application)
    }

    private fun getOperator() : String{
        return when (gameViewModel.menu.value!!) {
            1 -> context?.resources?.getString(R.string.operatorSum).toString()
            2 -> context?.resources?.getString(R.string.operatorSub).toString()
            3 -> context?.resources?.getString(R.string.operatorMuti).toString()
            else -> ""
        }
    }

    private fun resetStartComponent() {
        showTxtQuestion()
        enableAllBtn()
    }

    private fun enableAllBtn() {
        binding.apply {
            btnAnswer1.setEnabled(true)
            btnAnswer2.setEnabled(true)
            btnAnswer3.setEnabled(true)
            btnBackSumToMain.setEnabled(true)
        }
    }

    private fun disableAllBtn() {
        binding.apply {
            btnAnswer1.setEnabled(false)
            btnAnswer2.setEnabled(false)
            btnAnswer3.setEnabled(false)
            btnBackSumToMain.setEnabled(false)
        }
    }

    private fun resetStartBackground() {
        binding.apply {
            txtNumberResult.setTextColor(Color.BLACK)
            btnAnswer1.setBackgroundColor(resources.getColor(R.color.buttonInit))
            btnAnswer2.setBackgroundColor(resources.getColor(R.color.buttonInit))
            btnAnswer3.setBackgroundColor(resources.getColor(R.color.buttonInit))
        }
    }

    private fun updateBtnAnswer(choiceNumber: Int) {
        val question = gameViewModel.getQuestionValue()

        var btnAnswer = getCurrentBtnAnswer(choiceNumber)

        if (question?.checkAnswerCorrect(choiceNumber)!!) {
            disableAllBtn()
            changeBackgroundCorrect(btnAnswer)
            contDownForNewGame()
        } else {
            changeBackgroundWrong(btnAnswer)
        }
        btnAnswer.setEnabled(false)
        binding.invalidateAll()
    }

    private fun changeBackgroundWrong(btn: Button) {
        btn.setBackgroundColor(Color.RED)
        binding.txtNumberResult.setTextColor(resources.getColor(R.color.wrong))
    }

    private fun changeBackgroundCorrect(btn: Button) {
        btn.setBackgroundColor(Color.GREEN)
        binding.txtNumberResult.setTextColor(resources.getColor(R.color.correct))
    }

    private fun getCurrentBtnAnswer(choiceNumber:Int): Button {
        val btnAnswer = when(choiceNumber){
            1 -> binding.btnAnswer1
            2 -> binding.btnAnswer2
            3 -> binding.btnAnswer3
            else ->  binding.btnAnswer1
        }
        return btnAnswer
    }
    private fun contDownForNewGame() {
        showTxtTime()
        gameViewModel.startTimeQuestion()
    }

    private fun showTxtTime(){
        binding.apply {
            txtTime.visibility = View.VISIBLE
            txtNumber1.visibility = View.INVISIBLE
            txtNumber2.visibility = View.INVISIBLE
            txtOperator.visibility = View.INVISIBLE
        }
    }
    private fun showTxtQuestion(){
        binding.apply {
            txtTime.visibility = View.INVISIBLE
            txtNumber1.visibility = View.VISIBLE
            txtNumber2.visibility = View.VISIBLE
            txtOperator.visibility = View.VISIBLE
        }
    }

    private fun setBtnBackToMain() {
        binding.btnBackSumToMain.setOnClickListener {
            backToMain()
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            backToMain()
        }
    }
    private fun backToMain(){
        val navController = this@MathGameFragment.findNavController()
        navController.navigate(
            MathGameFragmentDirections.actionMathSumFragmentToChooseMapFragment(
                gameViewModel.player.value!!.playerId
            )
        )
        gameViewModel.stopTimeQuestion()
    }
}