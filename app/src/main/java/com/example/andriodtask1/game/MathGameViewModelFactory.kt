package com.example.andriodtask1.game

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.andriodtask1.database.player.PlayerDatabaseDao

class MathGameViewModelFactory(
    val menu: Int,
    val playerId:Long,
    val dataSource: PlayerDatabaseDao,
    val application: Application
):ViewModelProvider.Factory{


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MathGameViewModel::class.java)) {
            return MathGameViewModel(menu,playerId,dataSource,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}