package com.example.andriodtask1.database.player

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface PlayerDatabaseDao {
    @Insert
    suspend fun insert(player: Player)

    @Update
    suspend fun update(player:
                           Player)

    @Query("SELECT * from player WHERE playerId = :key")
    suspend fun get(key: Long): Player?

    @Query("DELETE FROM player")
    suspend fun clear()

    @Query("SELECT * FROM player ORDER BY playerId DESC LIMIT 1")
    suspend fun getPlayer(): Player?

    @Query("SELECT * FROM player ORDER BY amount_correct DESC")
    fun getAllPlayer(): LiveData<List<Player>>

    @Query("SELECT * FROM player ")
    suspend fun getTestAllPlayer(): List<Player>

    @Query("SELECT * FROM player WHERE name = :name LIMIT 1")
    suspend fun getPlayerByName(name:String?): Player?
}