package com.example.andriodtask1.database.playhistory

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface PlayHistoryDatabaseDao {
    @Insert
    suspend fun insert(night: PlayHistory)

    @Update
    suspend fun update(night:
                           PlayHistory)

    @Query("SELECT * from play_history WHERE playHistoryId = :key")
    suspend fun get(key: Long): PlayHistory?

    @Query("DELETE FROM play_history")
    suspend fun clear()

    @Query("SELECT * FROM play_history ORDER BY playHistoryId DESC LIMIT 1")
    suspend fun getPlayHistory(): PlayHistory?

    @Query("SELECT * FROM play_history ORDER BY playHistoryId DESC")
    fun getAllPlayHistory(): LiveData<List<PlayHistory>>
}