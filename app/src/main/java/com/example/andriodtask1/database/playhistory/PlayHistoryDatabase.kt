package com.example.andriodtask1.database.playhistory

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [PlayHistory::class], version = 1, exportSchema = false)
abstract class PlayHistoryDatabase : RoomDatabase() {

    abstract val playHistoryDao: PlayHistoryDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: PlayHistoryDatabase? = null

        fun getInstance(context: Context): PlayHistoryDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        PlayHistoryDatabase::class.java,
                        "play_history_database"
                    ).fallbackToDestructiveMigration()
                        .build()
                }
                return instance
            }
        }
    }
}