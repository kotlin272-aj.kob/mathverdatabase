package com.example.andriodtask1.database.player

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.andriodtask1.database.playhistory.PlayHistory

@Entity(tableName = "player"
)
//,
//foreignKeys = [
//@ForeignKey(
//    entity = PlayHistory::class,
//    parentColumns = ["playerId"],
//    childColumns = ["playHistoryId"]
//)
//]
data class Player (
    @PrimaryKey(autoGenerate = true)
    var playerId: Long= 0L,

    @ColumnInfo(name = "name")
    val name : String = "",

    @ColumnInfo(name = "amount_correct")
    var amountCorrect : Int = 0 ,

    @ColumnInfo(name = "amount_wrong")
    var amountWrong : Int = 0
){
    fun addCorrect(){
        amountCorrect++
    }
    fun addWrong(){
        amountWrong++
    }
}