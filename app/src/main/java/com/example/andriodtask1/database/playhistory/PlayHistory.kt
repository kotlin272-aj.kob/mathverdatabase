package com.example.andriodtask1.database.playhistory

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "play_history")

data class PlayHistory (
    @PrimaryKey(autoGenerate = true)
    var playHistoryId: Long= 0L,

    @ColumnInfo(name = "playerId")
    val playerId : String,

    @ColumnInfo(name = "amount_correct")
    val amountCorrect : Int ,

    @ColumnInfo(name = "amount_wrong")
    val amountWrong : Int
)