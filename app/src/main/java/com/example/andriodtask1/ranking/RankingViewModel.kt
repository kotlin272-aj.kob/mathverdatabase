package com.example.andriodtask1.ranking

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.example.andriodtask1.database.player.Player
import com.example.andriodtask1.database.player.PlayerDatabaseDao
import com.example.andriodtask1.formatPlayerList
import kotlinx.coroutines.launch


class RankingViewModel(
    val database: PlayerDatabaseDao,
    val application : Application
) : ViewModel(){
    val playerList : LiveData<List<Player>> = database.getAllPlayer()
    val playerListString = Transformations.map(playerList) { playerList ->
        formatPlayerList(playerList, application.resources)
    }

    private val _eventLoading = MutableLiveData<Boolean>()
    val eventLoading : LiveData<Boolean>
        get() = _eventLoading


    init {
        viewModelScope.launch {
            Log.i("Test Player", "Player init ${database.getTestAllPlayer()}")
        }
//        _eventLoading.value = true
    }
}