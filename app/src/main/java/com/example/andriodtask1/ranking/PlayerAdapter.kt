package com.example.andriodtask1.ranking

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.andriodtask1.R
import com.example.andriodtask1.database.player.Player

class PlayerAdapter: RecyclerView.Adapter<PlayerAdapter.ViewHolder>() {
    var data =  listOf<Player>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView){

        val nameText: TextView = itemView.findViewById(R.id.name_text)
        val scoreText: TextView = itemView.findViewById(R.id.score_text)
        val qualityImage: ImageView = itemView.findViewById(R.id.quality_image)

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.list_item_player, parent, false)
                return ViewHolder(view)
            }
        }
    }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        bind(holder, item)


    }

    fun bind(holder: ViewHolder, item: Player) {
        val res = holder.itemView.context.resources
        holder.nameText.text= String.format(res.getString(R.string.name_text), item.name);

        holder.scoreText.text= String.format(res.getString(R.string.score_text), item.amountCorrect , item.amountWrong);

        Log.i("Test Player","ชื่อผู้เล่น : ${item.name}")
        holder.qualityImage.setImageResource( getQualityImage(item.amountCorrect , item.amountWrong ) )
    }
    fun getQualityImage(amountCorrect : Int , amountWrong : Int): Int {
        if(amountCorrect >= amountWrong){
            return R.drawable.like
        }else{
            return R.drawable.dislike
        }
    }

}
