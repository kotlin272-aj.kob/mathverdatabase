package com.example.andriodtask1.ranking

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.andriodtask1.R
import com.example.andriodtask1.database.player.PlayerDatabase
import com.example.andriodtask1.databinding.FragmentRankingBinding

class RankingFragment : Fragment() {
    lateinit var binding: FragmentRankingBinding

    private lateinit var rankingViewModel: RankingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentRankingBinding>(
            inflater,
            R.layout.fragment_ranking, container, false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val rankingViewModelFactory = RankingViewModelFactory(dataSource,application)
        rankingViewModel = ViewModelProvider(this,rankingViewModelFactory).get(RankingViewModel::class.java)

        setHasOptionsMenu(true)

        val adapter = PlayerAdapter()
        binding.playerList.adapter = adapter
        binding.rankingViewModel = rankingViewModel
        binding.lifecycleOwner = this

        rankingViewModel.playerList.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })
        return binding.root
    }
}