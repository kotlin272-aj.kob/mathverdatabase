package com.example.andriodtask1.choosemap

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.andriodtask1.database.player.PlayerDatabaseDao

class ChooseMapViewModelFactory(
    private val playerId: Long,
    private val dataSource: PlayerDatabaseDao
):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChooseMapViewModel::class.java)) {
            return ChooseMapViewModel(playerId,dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}