package com.example.andriodtask1.choosemap

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.andriodtask1.database.player.Player
import com.example.andriodtask1.database.player.PlayerDatabaseDao
import kotlinx.coroutines.launch

class ChooseMapViewModel(
    val playerId : Long,
    val database: PlayerDatabaseDao
) : ViewModel(){
    private val _player = MutableLiveData<Player>()
    val player : LiveData<Player>
        get()=_player

    private val _eventChooseSum = MutableLiveData<Boolean>()
    val eventChooseSum : LiveData<Boolean>
        get() = _eventChooseSum

    private val _eventChooseSub = MutableLiveData<Boolean>()
    val eventChooseSub : LiveData<Boolean>
        get() = _eventChooseSub

    private val _eventChooseMulti = MutableLiveData<Boolean>()
    val eventChooseMulti : LiveData<Boolean>
        get() = _eventChooseMulti

    private val _eventLoading = MutableLiveData<Boolean>()
    val eventLoading : LiveData<Boolean>
        get() = _eventLoading

    fun getPlayerValue() = _player.value

    fun onChooseSum(){
        _eventChooseSum.value = true
    }
    fun onChooseSumFinish(){
        _eventChooseSum.value = false
    }

    fun onChooseSub(){
        _eventChooseSub.value = true
    }
    fun onChooseSubFinish(){
        _eventChooseSub.value = false
    }

    fun onChooseMulti(){
        _eventChooseMulti.value = true
    }
    fun onChooseMultiFinish(){
        _eventChooseMulti.value = false
    }
    init{
        viewModelScope.launch {
            _player.value = database.get(playerId) ?: Player()
            _eventLoading.value = true
        }
    }
}