package com.example.andriodtask1.choosemap

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.andriodtask1.R
import com.example.andriodtask1.database.player.PlayerDatabase
import com.example.andriodtask1.databinding.FragmentChooseMapBinding
import com.example.andriodtask1.game.MathGameFragmentDirections

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChooseMapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChooseMapFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private lateinit var chooseMapViewModel : ChooseMapViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentChooseMapBinding>(
            inflater,
            R.layout.fragment_choose_map, container, false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao

        val args = ChooseMapFragmentArgs.fromBundle(requireArguments())

        val chooseMapViewModelFactory = ChooseMapViewModelFactory(args.playerId,dataSource)
        chooseMapViewModel = ViewModelProvider(this,chooseMapViewModelFactory).get(ChooseMapViewModel::class.java)
        
        setHasOptionsMenu(true)


        chooseMapViewModel.eventChooseSum.observe(viewLifecycleOwner, Observer { hasClick ->
            if(hasClick){
                val navController = this@ChooseMapFragment.findNavController()
                navController.navigate(
                    ChooseMapFragmentDirections.actionChooseMapFragmentToMathGameFragment(
                        1,
                        chooseMapViewModel.playerId
                                            )
                )
                chooseMapViewModel.onChooseSumFinish()
            }
        })
        chooseMapViewModel.eventChooseSub.observe(viewLifecycleOwner, Observer { hasClick ->
            if(hasClick){
                val navController = this@ChooseMapFragment.findNavController()
                navController.navigate(
                    ChooseMapFragmentDirections.actionChooseMapFragmentToMathGameFragment(
                        2,
                        chooseMapViewModel.playerId
                    )
                )
                chooseMapViewModel.onChooseSubFinish()
            }
        })
        chooseMapViewModel.eventChooseMulti.observe(viewLifecycleOwner, Observer { hasClick ->
            if(hasClick){
                val navController = this@ChooseMapFragment.findNavController()
                navController.navigate(
                    ChooseMapFragmentDirections.actionChooseMapFragmentToMathGameFragment(
                        3,
                        chooseMapViewModel.playerId
                    )
                )
                chooseMapViewModel.onChooseMultiFinish()
            }
        })
        chooseMapViewModel.eventLoading.observe(viewLifecycleOwner, Observer { hasLoading ->
            if(hasLoading){
                binding.invalidateAll()
            }
        })
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            val navController = this@ChooseMapFragment.findNavController()
            navController.navigate(
                ChooseMapFragmentDirections.actionChooseMapFragmentToTitleFragment()
            )
        }
        binding.chooseMapViewModel = chooseMapViewModel
        return binding.root
    }
}
