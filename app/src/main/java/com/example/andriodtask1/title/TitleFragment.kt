package com.example.andriodtask1.title

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.andriodtask1.R
import com.example.andriodtask1.choosemap.ChooseMapViewModel
import com.example.andriodtask1.database.player.PlayerDatabase
import com.example.andriodtask1.title.TitleFragmentDirections
import com.example.andriodtask1.title.TitleViewModel
import com.example.andriodtask1.databinding.FragmentTitleBinding
import com.example.andriodtask1.game.MathGameViewModelFactory


class TitleFragment : Fragment() {

    private lateinit var titleViewModel : TitleViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(
            inflater,
            R.layout.fragment_title, container, false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val titleViewModelFactory = TitleViewModelFactory(dataSource)
        titleViewModel = ViewModelProvider(this,titleViewModelFactory).get(TitleViewModel::class.java)

        setHasOptionsMenu(true)

        titleViewModel.eventSubmit.observe(viewLifecycleOwner, Observer { hasClick ->
            if(hasClick){

                Log.i("Test Player", "PlayerId  ${titleViewModel.playerId}")

                val navController = this@TitleFragment.findNavController()
                navController.navigate(
                    TitleFragmentDirections.actionTitleFragmentToChooseMapFragment(
                        titleViewModel.playerId
                    )
                )
                hideKeyBoard()
                titleViewModel.onSubmitFinish()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        binding.titleViewModel = titleViewModel
        return binding.root
    }

    private fun hideKeyBoard(){
        val inputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken,0)
    }
}