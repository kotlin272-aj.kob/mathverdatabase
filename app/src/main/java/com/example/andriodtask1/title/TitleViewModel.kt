package com.example.andriodtask1.title


import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.adapters.TextViewBindingAdapter.setPassword
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.andriodtask1.database.player.Player
import com.example.andriodtask1.database.player.PlayerDatabaseDao
import kotlinx.coroutines.launch


class TitleViewModel(
    val database: PlayerDatabaseDao

) : ViewModel(){
    var name :String = ""
    var playerId : Long = -1

    private val _eventSubmit = MutableLiveData<Boolean>()
    val eventSubmit : LiveData<Boolean>
        get() = _eventSubmit

    fun onSubmit(){
        var player: Player? = Player(-1,"")
        viewModelScope.launch {
            player = database.getPlayerByName(name) ?: null
            Log.i("Test Player", "Success Scope")

            if(player == null){
                val playerInsert = Player(name = name)
                Log.i("Test Player", "Name : ${name!!}" )
                database.insert(playerInsert)

                player = database.getPlayerByName(name) ?: null
            }
            Log.i("Test Player", "PlayerId is ${player?.playerId.toString()}")
            Log.i("Test Player", "Name is ${player?.name!!}" )
            playerId = player!!.playerId
            _eventSubmit.value = true
        }
    }
    fun onSubmitFinish(){
        _eventSubmit.value = false
    }

    init {
        name = ""
    }
}